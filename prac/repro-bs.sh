#!/bin/bash

# Usage: ./repro-bs.sh [niters]

nitrs=1
if [ "$1" != "" ]; then
    nitrs="$1"
fi

# Check whether we have enough RAM to do the two largest experiments
if [ "$FLORAM_MAXGB" -gt 96 ]; then
    large_exps="28 30"
elif [ "$FLORAM_MAXGB" -gt 24 ]; then
    large_exps="28"
else
    large_exps=""
fi

cd ..
mkdir -p prac/data
for itr in $(seq 1 $nitrs); do
    for size in 16 18 20 22 24 26 ${large_exps}; do
        now=`date`; echo "$now: Running 1 search on DB of size 2^${size} ..."
        ./run-experiment bs ${size} 1 >> prac/data/log_${size}_1_bs.out
    done
done
