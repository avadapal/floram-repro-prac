#!/bin/bash

# Usage: ./repro-reads.sh [niters]

nitrs=1
if [ "$1" != "" ]; then
    nitrs="$1"
fi

# Check whether we have enough RAM to do the two largest experiments
if [ "$FLORAM_MAXGB" -gt 128 ]; then
    large_exps="28 30"
elif [ "$FLORAM_MAXGB" -gt 32 ]; then
    large_exps="28"
else
    large_exps=""
fi

cd ..
mkdir -p prac/data
for itr in $(seq 1 $nitrs); do
    for size in 16 18 20 22 24 26 ${large_exps}; do
        now=`date`; echo "$now: Running 10 reads on DB of size 2^${size} ..."
        ./run-experiment read ${size} 10 >> prac/data/log_${size}_10_reads.out
    done
done

