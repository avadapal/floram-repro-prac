#!/bin/bash

# Usage: ./repro-bs-const-db.sh [niters]

nitrs=1
if [ "$1" != "" ]; then
    nitrs="$1"
fi

cd ..
mkdir -p prac/data
for itr in $(seq 1 $nitrs); do
    for n in 4 8 16 32 64; do
        now=`date`; echo "$now: Running ${n} searches on DB of size 2^20 ..."
        ./run-experiment bs 20 ${n} >> prac/data/log_20_${n}_bs.out
    done
done
