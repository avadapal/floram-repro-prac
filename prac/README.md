## Building the docker

cd docker
Build the docker image with ./build-docker
Start the dockers with ./start-docker
This will start three dockers, each running one of the parties.

## Setting the network parameters

./set-networking 30ms 100mbit

## Generating raw data for the binary search experiments

cd prac

### Generating data for Figure 7 a)
Open the file `repro-bs-const-db.sh`
Change the variable `nitrs` to the number of times you would like run each experiment
Generate the raw data for Figure 7a) using `sh repro-bs-const-db.sh`

### Generating data for Figures 7 b) and 7 c)
Open the file `repro-bs.sh`
Change the variable `nitrs` to the number of times you would like run each experiment
Generate the raw data for Figure 7b) and 7c) using `sh repro-bs.sh`
